/************************************************************************
* floor_odom
* Stereo odometry from a downward-facing stereo camera on a vehicle
* Alankar Kotwal <alankarkotwal13@gmail.com>
* Created June 8, edited June 10
*************************************************************************/

// Include files: ROS headers
#include <ros/ros.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/PoseStamped.h>

// OpenCV
#include <opencv2/opencv.hpp>

/************************************************************************/

// Global variables
cv::Mat left_image, right_image, left_image_init, right_image_init;
bool have_left_image = false, have_right_image = false, first = true;

// Odometry parameters
float depth;
float depth_init;
float camera_x = 0, camera_y = 0;

// Publishers
ros::Publisher odom_pub;

// Camera parameters
float focal_length = 2e-3;
float baseline = 3e-2;
float pix_res = 6e-6;

// Matching parameters
// -- More to come when we implement RANSAC for this -- //

/************************************************************************/

// Functions
void updateDepth() {
	
	/*
		Get stereo depth assuming an almost-planar surface
		For now, we assume a completely planar surface nearly parallel to
		the sensor plane
	*/
	
	// Construct the ROI and the template from the right image
	// Modify this later for a RANSAC-based method
	cv::Rect window((float)right_image.cols/4, (float)right_image.rows/4,
					(float)right_image.cols/2, (float)right_image.rows/2);
	cv::Mat temp_image = right_image(window);
	
	// Match the template to the left using normalised cross-correlation
	cv::Mat corrs;
	cv::matchTemplate(left_image, temp_image, corrs, CV_TM_CCOEFF_NORMED);
	
	// Get the maximum and hence the disparity
	cv::Point max;
	cv::minMaxLoc(corrs, NULL, NULL, NULL, &max, cv::Mat());
	depth = 100*focal_length*baseline/(pix_res*(max.x - corrs.cols/2));
	//ROS_INFO("I detected the depth as %f cm.", depth);
	
	// Show stuff
	//cv::imshow("Left", left_image);
	//cv::imshow("Right", right_image);
	//cv::circle(corrs, max, 5, cv::Scalar(0, 0, 0), -1, 8, 0);
	//cv::imshow("Correlation", corrs);
	//cv::waitKey(1);
	
}

void updateOdometry() {

	/*
		Update odometry using the current left-previous left pair
		For now I assume there is no rotation/scale. We will try 
		accounting for scale using the depth information from the
		above method.
	*/
	
	// Account for scale
	//resize(left_image_init, left_image_init, cv::Size(0, 0), depth_init/depth, depth_init/depth, cv::INTER_LINEAR);
	
	// Construct the ROI and the template from the previous left image
	// Modify this later for a RANSAC-based method
	cv::Rect window((float)left_image.cols/4, (float)left_image.rows/4,
					(float)left_image.cols/2, (float)left_image.rows/2);
	cv::Mat temp_image = left_image_init(window);
	
	// Match the template to the left using normalised cross-correlation
	cv::Mat corrs;
	cv::matchTemplate(left_image, temp_image, corrs, CV_TM_CCOEFF_NORMED);
	
	// Get the maximum and hence the odometry
	cv::Point max;
	cv::minMaxLoc(corrs, NULL, NULL, NULL, &max, cv::Mat());
	//ROS_INFO("I found the maximum at (%d, %d)", max.x, max.y);
	
	float disp_x, disp_y;
	disp_x = max.x - corrs.cols/2;
	disp_y = max.y - corrs.rows/2;
	camera_x += depth*pix_res*disp_x/focal_length;
	camera_y += depth*pix_res*disp_y/focal_length;
	
	ROS_INFO("My co-ordinates are (%f, %f, %f) cm.", camera_x, camera_y, depth);
	
	// Show stuff
	//cv::imshow("PrevLeft", left_image_init);
	//cv::imshow("PrevRight", right_image_init);
	cv::circle(corrs, max, 5, cv::Scalar(0, 0, 0), -1, 8, 0);
	cv::imshow("Correlation", corrs);
	cv::waitKey(1);
	
	geometry_msgs::PoseStamped odom;
	odom.header.stamp = ros::Time::now();
	odom.header.frame_id = "map";
	
	// Set the position
    odom.pose.position.x = camera_x/100;
    odom.pose.position.y = camera_y/100;
    odom.pose.position.z = depth/100;
    odom.pose.orientation.x = 0.0;
    odom.pose.orientation.y = 0.0;
    odom.pose.orientation.z = 0.0;
    odom.pose.orientation.w = 1.0;

    // Publish the message
    odom_pub.publish(odom);

}

/************************************************************************/

// Callbacks

// Left image callback
void leftImageCb(const sensor_msgs::ImageConstPtr& msg) {
	
	cv_bridge::CvImagePtr cv_ptr;
	
	try {
		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}
	
	left_image = cv_ptr->image;
	
	if(have_right_image) {
		updateDepth();
		if(!first) {
			updateOdometry();
		} else {
			first = false;
		}
		left_image_init = left_image;
		right_image_init = right_image;
		depth_init = depth;
		have_right_image = false;
		have_left_image = false;
	}
	else {
		have_left_image = true;
	}

	// cv::imshow("Left", cv_ptr->image);
	// cv::waitKey(1);
}

// Right image callback
void rightImageCb(const sensor_msgs::ImageConstPtr& msg) {
	
	cv_bridge::CvImagePtr cv_ptr;
	
	try {
		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}
	
	right_image = cv_ptr->image;
	
	if(have_left_image) {
		updateDepth();
		if(!first) {
			updateOdometry();
		} else {
			first = false;
		}
		left_image_init = left_image;
		right_image_init = right_image;
		depth_init = depth;
		have_right_image = false;
		have_left_image = false;
	}
	else {
		have_right_image = true;
	}

	// cv::imshow("Right", cv_ptr->image);
	// cv::waitKey(1);
}

/************************************************************************/

// Main

int main(int argc, char** argv) {
	
	ros::init(argc, argv, "floor_odom");
	ros::NodeHandle nh;
	
	image_transport::ImageTransport it(nh);
	image_transport::Subscriber left_image_sub, right_image_sub;
	left_image_sub = it.subscribe("/duo3d_camera/left/image_rect", 1, &leftImageCb);
	right_image_sub = it.subscribe("/duo3d_camera/right/image_rect", 1, &rightImageCb);
	
	odom_pub = nh.advertise<geometry_msgs::PoseStamped>("odom", 50);
	
	ROS_INFO("All translation measurements have their origin at the point directly below the camera in the first position. All rotation measurements have their z-axis along the vertically downward direction and their x- and y-axes parallel to the camera axes in the first frame.");
	
	ros::spin();

	return 0;
}
